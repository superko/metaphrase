import Vue from "vue";
import App from "./App.vue";

// Custom HTML elements
import Button from "./elements/Button";
import Dialog from "./elements/Dialog";
Vue.component("Button", Button);
Vue.component("Dialog", Dialog);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
