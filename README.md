# Metaphrase

A frontend tool to manage paragraph-by-paragraph translations. https://www.superko.org/metaphrase

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn run serve
```

### Compiles and minifies for production

```
yarn run build
```

### Run your tests

```
yarn run test
```

### Lints and fixes files

```
yarn run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### Featurelist TODO

Editing:

- remove a language
- reorder languages
- add paragraph anywhere
- remove paragraphs
- reorder paragraphs

I/O:

- save/load on localStorage
- save/load on server

a11y:

- keyboard shortcuts
- arrow key navigation through paragraphs
- adjustable text size

style:

- overall layout/spacing
- fonts
- button states
- switch to Sass + implement variables
- (sigh) use `<table>`. it's the best choice here
- show errors to user

code:

- quite a lot of repeated code for passing state. use VueX?
- TypeScript??????? this project seems ripe for it.

nth/investigate:

- formatting? markdown?
